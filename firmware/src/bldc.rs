use core::cell::RefCell;

use cortex_m::interrupt::Mutex;
use cortex_m_rt_macros::interrupt as isr;

use stm32g0::stm32g07x::interrupt;
use stm32g0::stm32g07x::RCC;
use stm32g0::stm32g07x::TIM1;
use stm32g0::stm32g07x::EXTI;
use stm32g0::stm32g07x::GPIOA;

use cortex_m::peripheral::NVIC;
use stm32g0xx_hal::gpio::gpioa::*;
use stm32g0xx_hal::gpio::gpiob::*;
use stm32g0xx_hal::gpio::{DefaultMode, Floating, Input, Output, PushPull, SignalEdge};
use stm32g0xx_hal::hal::digital::v2::{InputPin, OutputPin};
use stm32g0xx_hal::rcc::Rcc;
use stm32g0xx_hal::timer::pins::TimerPin;

static BLDC: Mutex<RefCell<Option<Bldc>>> = Mutex::new(RefCell::new(None));

pub fn init(
    u_low: PA7<Output<PushPull>>,
    u_high: PA8<DefaultMode>,
    u_hall: PA0<Input<Floating>>,

    v_low: PB0<Output<PushPull>>,
    v_high: PA9<DefaultMode>,
    v_hall: PA1<Input<Floating>>,

    w_low: PB1<Output<PushPull>>,
    w_high: PA10<DefaultMode>,
    w_hall: PA2<Input<Floating>>,

    timer: TIM1,
    exti: EXTI,
) {
    cortex_m::interrupt::free(|cs| {
        BLDC.borrow(cs).replace(Some(Bldc::new(
            u_low, u_high, u_hall, v_low, v_high, v_hall, w_low, w_high, w_hall, timer, exti
        )));
    });

    cortex_m::interrupt::free(|cs| {
        if let Ok(mut bldc) = BLDC.borrow(cs).try_borrow_mut() {
            if let Some(bldc) = bldc.as_mut() {
                bldc.duty_cycle(0);
                bldc.enable();
            }
        }
    });
}

pub fn duty_cycle(duty_cycle: u16) {
    cortex_m::interrupt::free(|cs| {
        if let Ok(mut bldc) = BLDC.borrow(cs).try_borrow_mut() {
            if let Some(bldc) = bldc.as_mut() {
                bldc.duty_cycle(duty_cycle);
            }
        }
    })
}

pub fn run() {
    cortex_m::interrupt::free(|cs| {
        if let Ok(mut bldc) = BLDC.borrow(cs).try_borrow_mut() {
            if let Some(bldc) = bldc.as_mut() {
                bldc.run()
            }
        }
    })
}

pub fn count() -> Option<u32> {
    let mut count = None;

    cortex_m::interrupt::free(|cs| {
        if let Ok(mut bldc) = BLDC.borrow(cs).try_borrow_mut() {
            if let Some(bldc) = bldc.as_mut() {
                count = Some(bldc.count())
            }
        }
    });

    count
}

#[derive(PartialEq, Copy, Clone)]
enum Step {
    Step1,
    Step2,
    Step3,
    Step4,
    Step5,
    Step6,
}

struct Bldc {
    u_low: PA7<Output<PushPull>>,
    u_high: PA8<DefaultMode>,
    u_hall: PA0<Input<Floating>>,

    v_low: PB0<Output<PushPull>>,
    v_high: PA9<DefaultMode>,
    v_hall: PA1<Input<Floating>>,

    w_low: PB1<Output<PushPull>>,
    w_high: PA10<DefaultMode>,
    w_hall: PA2<Input<Floating>>,

    timer: TIM1,
    exti: EXTI,

    last_step: Step,
    count: u32,
}

impl Bldc {
    fn new(
        u_low: PA7<Output<PushPull>>,
        u_high: PA8<DefaultMode>,
        u_hall: PA0<Input<Floating>>,

        v_low: PB0<Output<PushPull>>,
        v_high: PA9<DefaultMode>,
        v_hall: PA1<Input<Floating>>,

        w_low: PB1<Output<PushPull>>,
        w_high: PA10<DefaultMode>,
        w_hall: PA2<Input<Floating>>,

        timer: TIM1,
        exti: EXTI,
    ) -> Bldc {
        // unsafe because Rcc.rb is private
        let rcc = unsafe { &(*RCC::ptr()) };
        rcc.apbenr2.modify(|_, w| w.tim1en().set_bit());

        TimerPin::setup(&u_high);
        //let u_high = u_high.into_push_pull_output();

        TimerPin::setup(&v_high);
        //let v_high = v_high.into_push_pull_output();

        TimerPin::setup(&w_high);
        //let w_high = w_high.into_push_pull_output();

        // Timer 1 capture compare enable register
        timer.ccer.write(|w| {
            // Disable ccr1 output
            w.cc1e().clear_bit();
            // Disable ccr2 output
            w.cc2e().clear_bit();
            // Disable ccr3 output
            w.cc3e().clear_bit();
            // Enable ccr4 for the interrupt
            w.cc4e().set_bit();
            w
        });

        // Timer 1 capture compare mode register 1
        timer.ccmr1_output_mut().write(|w| unsafe {
            // Set the capture compare to output
            w.cc1s().bits(0b00);
            // Enable ccr register preloading. Waits to load the registers until an update event
            w.oc1pe().set_bit();
            // Set the capture compare mode to PWM1
            w.oc1m().bits(0b110);
            // for whatever reason, bit 3 of the mode bits is separate
            w.oc1m_3().clear_bit();

            // Set the capture compare to output
            w.cc2s().bits(0b00);
            // Enable ccr register preloading. Waits to load the registers until an update event
            w.oc2pe().set_bit();
            // Set the capture compare mode to PWM1
            w.oc2m().bits(0b110);
            // for whatever reason, bit 3 of the mode bits is separate
            w.oc2m_3().clear_bit();
            w
        });

        // Timer 1 capture compare mode register 2
        timer.ccmr2_output_mut().write(|w| unsafe {
            // Set the capture compare to output
            w.cc3s().bits(0b00);
            // Enable ccr register preloading. Waits to load the registers until an update event
            w.oc3pe().set_bit();
            // Set the capture compare mode to PWM1
            w.oc3m().bits(0b110);
            // for whatever reason, bit 3 of the mode bits is separate
            w.oc3m_3().clear_bit();

            // Setup cc4 to trigger the interrupt
            // Enable ccr register preloading. Waits to load the registers until an update event
            w.oc4pe().set_bit();
            // Set the capture compare mode to frozen, so it does not affect the output
            w.oc4m().bits(0b000);
            w.oc4m_3().clear_bit();
            w
        });

        // Timer 1 auto-reload register
        timer.arr.write(|w| unsafe { w.arr().bits(0x01ff) });

        // Timer 1 capture compare register 1
        timer.ccr1.write(|w| unsafe { w.ccr1().bits(0x0000) });
        // Timer 1 capture compare register 2
        timer.ccr2.write(|w| unsafe { w.ccr2().bits(0x0000) });
        // Timer 1 capture compare register 3
        timer.ccr3.write(|w| unsafe { w.ccr3().bits(0x0000) });
        // Timer 1 capture compare register 4
        // Set it to fire a little before the pwm goes high so that we can read the
        // bemf. Needs to account for delays in the interrupt
        timer.ccr4.modify(|_, w| unsafe { w.ccr4().bits(90) });

        timer.egr.write(|w| w.ug().set_bit());

        // Timer 1 prescaler
        timer.psc.write(|w| unsafe { w.psc().bits(0x0000) });

        timer.cnt.write(|w| unsafe { w.cnt().bits(0x0000) });

        // Set moe and dead time
        timer.bdtr.write(|w| unsafe {
            w.moe().set_bit() /*.dtg().bits(0x01)*/
        });

        // Enable the timer
        timer.cr1.write(|w| w.arpe().set_bit().cen().set_bit());

        exti.rtsr1.modify(|_, w| {
            w.tr0().enabled();
            w.tr1().enabled();
            w.tr2().enabled();
            w
        });

        exti.ftsr1.modify(|_, w| {
            w.tr0().enabled();
            w.tr1().enabled();
            w.tr2().enabled();
            w
        });

        exti.exticr1.modify(|_, w| {
            w.exti0_7().pa();
            w.exti8_15().pa();
            w.exti16_23().pa();
            w
        });

        exti.imr1_mut().modify(|_, w| {
            w.im0().set_bit();
            w.im1().set_bit();
            w.im2().set_bit();
            w
        });

        Bldc {
            u_low,
            u_high,
            u_hall,

            v_low,
            v_high,
            v_hall,

            w_low,
            w_high,
            w_hall,

            timer,
            exti,

            last_step: Step::Step1,
            count: 0,
        }
    }

    fn count(&mut self) -> u32 {
        let count = self.count;
        self.count = 0;
        count
    }

    fn enable(&self) {
        unsafe { NVIC::unmask(interrupt::EXTI0_1); }
        unsafe { NVIC::unmask(interrupt::EXTI2_3); }
    }

    fn duty_cycle(&self, duty_cycle: u16) {
        self.timer
            .ccr1
            .modify(|_, w| unsafe { w.ccr1().bits(duty_cycle) });
        self.timer
            .ccr2
            .modify(|_, w| unsafe { w.ccr2().bits(duty_cycle) });
        self.timer
            .ccr3
            .modify(|_, w| unsafe { w.ccr3().bits(duty_cycle) });
    }

    fn hall_step(&self) -> Option<Step> {
        if let (Ok(u_hall), Ok(v_hall), Ok(w_hall)) = (
            self.u_hall.is_high(),
            self.v_hall.is_high(),
            self.w_hall.is_high(),
        ) {
            match (u_hall, v_hall, w_hall) {
                (true, false, false) => Some(Step::Step1),
                (true, true, false) => Some(Step::Step2),
                (false, true, false) => Some(Step::Step3),
                (false, true, true) => Some(Step::Step4),
                (false, false, true) => Some(Step::Step5),
                (true, false, true) => Some(Step::Step6),

                (true, true, true) => None,
                (false, false, false) => None,
            }
        } else {
            None
        }
    }

    fn apply_step(&mut self, step: Step) {
        if step != self.last_step {
            self.count += 1;
            self.last_step = step;
        }

        match step {
            Step::Step1 => {
                self.u_high();
                self.v_low();
                self.w_float();
            }
            Step::Step2 => {
                self.u_high();
                self.v_float();
                self.w_low();
            }
            Step::Step3 => {
                self.u_float();
                self.v_high();
                self.w_low();
            }
            Step::Step4 => {
                self.u_low();
                self.v_high();
                self.w_float();
            }
            Step::Step5 => {
                self.u_low();
                self.v_float();
                self.w_high();
            }
            Step::Step6 => {
                self.u_float();
                self.v_low();
                self.w_high();
            }
        }
    }

    fn clear_interrupt(&mut self) {
        self.exti.rpr1.modify(|_, w| {
            w.rpif0().set_bit();
            w.rpif1().set_bit();
            w.rpif2().set_bit();
            w
        });

        self.exti.fpr1.modify(|_, w| {
            w.fpif0().set_bit();
            w.fpif1().set_bit();
            w.fpif2().set_bit();
            w
        });
    }

    fn run(&mut self) {
        if let Some(step) = self.hall_step() {
            self.apply_step(step);
        } else {
            self.u_float();
            self.v_float();
            self.w_float();
        }
    }

    fn u_high(&mut self) {
        // Enable ccr1 output for u high
        self.timer.ccer.modify(|_, w| w.cc1e().set_bit());

        // Disable u low
        self.u_low.set_low().ok();
    }

    fn u_low(&mut self) {
        // Disable ccr1 output for u high
        self.timer.ccer.modify(|_, w| w.cc1e().clear_bit());

        // Enable u low
        self.u_low.set_high().ok();
    }

    fn u_float(&mut self) {
        // Disable ccr1 output for u high
        self.timer.ccer.modify(|_, w| w.cc1e().clear_bit());

        // Disable u low
        self.u_low.set_low().ok();
    }

    fn v_high(&mut self) {
        // Enable ccr1 ovtpvt for v high
        self.timer.ccer.modify(|_, w| w.cc2e().set_bit());

        // Disable v low
        self.v_low.set_low().ok();
    }

    fn v_low(&mut self) {
        // Disable ccr1 ovtpvt for v high
        self.timer.ccer.modify(|_, w| w.cc2e().clear_bit());

        // Enable v low
        self.v_low.set_high().ok();
    }

    fn v_float(&mut self) {
        // Disable ccr1 ovtpvt for v high
        self.timer.ccer.modify(|_, w| w.cc2e().clear_bit());

        // Disable v low
        self.v_low.set_low().ok();
    }

    fn w_high(&mut self) {
        // Enable ccr1 owtpwt for w high
        self.timer.ccer.modify(|_, w| w.cc3e().set_bit());

        // Disable w low
        self.w_low.set_low().ok();
    }

    fn w_low(&mut self) {
        // Disable ccr1 owtpwt for w high
        self.timer.ccer.modify(|_, w| w.cc3e().clear_bit());

        // Enable w low
        self.w_low.set_high().ok();
    }

    fn w_float(&mut self) {
        // Disable ccr1 owtpwt for w high
        self.timer.ccer.modify(|_, w| w.cc3e().clear_bit());

        // Disable w low
        self.w_low.set_low().ok();
    }
}

#[allow(non_snake_case)]
#[isr]
fn EXTI0_1() {
    cortex_m::interrupt::free(|cs| {
        if let Ok(mut bldc) = BLDC.borrow(cs).try_borrow_mut() {
            if let Some(bldc) = bldc.as_mut() {
                bldc.run();
                bldc.clear_interrupt();
            }
        }
    })
}

#[allow(non_snake_case)]
#[isr]
fn EXTI2_3() {
    cortex_m::interrupt::free(|cs| {
        if let Ok(mut bldc) = BLDC.borrow(cs).try_borrow_mut() {
            if let Some(bldc) = bldc.as_mut() {
                bldc.run();
                bldc.clear_interrupt();
            }
        }
    })
}
