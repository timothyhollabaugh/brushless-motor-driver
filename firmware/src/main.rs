#![no_std]
#![no_main]

extern crate panic_halt; // you can put a breakpoint on `rust_begin_unwind` to catch panics

mod bldc;
mod uart;

use core::fmt::Write as _;
use core::str;
use core::cell::RefCell;

use cortex_m::interrupt::Mutex;
use cortex_m_rt::entry;
use cortex_m_rt_macros::interrupt as isr;

use embedded_hal::serial::{Read, Write};
use embedded_hal::digital::v2::InputPin;

use stm32g0::stm32g07x;
use stm32g0::stm32g07x::interrupt;

use cortex_m::peripheral::SYST;
use stm32g0xx_hal::gpio::{GpioExt, Input, Floating};
use stm32g0xx_hal::hal::digital::v2::OutputPin;
use stm32g0xx_hal::rcc::RccExt;
use stm32g0xx_hal::serial::{Config, SerialExt};
use stm32g0xx_hal::time::U32Ext;
use stm32g0xx_hal::gpio::gpioa::PA6;
use stm32g0xx_hal::gpio::DefaultMode;
use stm32g0xx_hal::timer::pins::TimerPin;

const MIN_DUTY: u8 = 0;
const MAX_DUTY: u8 = 1;

const RANGE_DUTY: u8 = MAX_DUTY - MIN_DUTY;

static PWM_IN: Mutex<RefCell<Option<PwmIn>>> = Mutex::new(RefCell::new(None));

enum PwmInRead {
    Low,
    High,
    Pwm { period: u32, high_time: u32 } 
}

fn init_pwm(pwm: PA6<Input<Floating>>, tim3: stm32g07x::TIM3) {
    cortex_m::interrupt::free(|cs| {
        PWM_IN.borrow(cs).replace(Some(PwmIn::setup(pwm, tim3)));
    });
}

fn read_pwm() -> PwmInRead {
    cortex_m::interrupt::free(|cs| {
        if let Ok(mut pwm_in) = PWM_IN.borrow(cs).try_borrow_mut() {
            if let Some(pwm_in) = pwm_in.as_mut() {
                pwm_in.read_pwm()
            } else {
                PwmInRead::Low
            }
        } else {
            PwmInRead::Low
        }
    })
}

fn count_pwm() -> u32 {
    cortex_m::interrupt::free(|cs| {
        if let Ok(mut pwm_in) = PWM_IN.borrow(cs).try_borrow_mut() {
            if let Some(pwm_in) = pwm_in.as_mut() {
                pwm_in.count
            } else {
                0
            }
        } else {
            0
        }
    })
}

struct PwmIn {
    pwm: PA6<Input<Floating>>,
    timer: stm32g07x::TIM3,
    timeout: Option<bool>,
    count: u32,
}

impl PwmIn {
    fn setup(pwm: PA6<DefaultMode>, tim3: stm32g07x::TIM3) -> PwmIn {
        TimerPin::<stm32g07x::TIM3>::setup(&pwm);

        // Wire TI1 to both CCR1 and CCR2, where CCR1 has the PWM period and CCR2 has the pwm high
        // time

        tim3.psc.write(|w| unsafe {
            w.psc().bits(0x0001);
            w
        });

        tim3.ccmr1_input_mut().write(|w| unsafe {

            // Select TI1 as input for CCR1
            w.cc1s().bits(0b01);

            // Select TI1 as input for CCR2
            w.cc2s().bits(0b10);
            w
        });

        tim3.ccer.write(|w| {
            // Set the CCR1 input polarity to rising edge
            w.cc1p().clear_bit();
            w.cc1np().clear_bit();

            // Set the CCR2 input polarity to falling edge
            w.cc2p().set_bit();
            w.cc2np().clear_bit();
            w
        });

        tim3.smcr.write(|w| unsafe {
            // Select TI1 as the trigger
            w.ts().bits(0b00101);

            // Reset the timer on trigger
            w.sms().bits(0b100);
            w
        });

        tim3.arr.write(|w| unsafe {
            w.bits(0xffff_ffff);
            w
        });

        tim3.ccer.modify(|_, w| {
            // Enable the capture compares
            w.cc1e().set_bit();
            w.cc2e().set_bit();
            w
        });

        tim3.cr1.write(|w| {
            w.cen().set_bit();
            w
        });

        unsafe { stm32g07x::NVIC::unmask(interrupt::TIM3) }

        tim3.dier.write(|w| {
            w.uie().set_bit();
            w.tie().set_bit();
            w
        });

        PwmIn {
            pwm,
            timer: tim3,
            timeout: None,
            count: 0
        }
    }

    fn read_pwm(&self) -> PwmInRead {
        if let Some(timeout) = self.timeout {
            if timeout {
                PwmInRead::High 
            } else {
                PwmInRead::Low
            }
        } else {
            let period = self.timer.ccr1.read().bits();
            let high_time = self.timer.ccr2.read().bits();
            PwmInRead::Pwm {
                period,
                high_time,
            }
        }
    }
}

#[allow(non_snake_case)]
#[isr]
fn TIM3() {
    cortex_m::interrupt::free(|cs| {
        if let Ok(mut pwm_in) = PWM_IN.borrow(cs).try_borrow_mut() {
            if let Some(pwm_in) = pwm_in.as_mut() {
                if pwm_in.timer.sr.read().uif().bit_is_set() {
                    pwm_in.timeout = if let Ok(true) =  pwm_in.pwm.is_low() {
                        Some(false)
                    } else {
                        Some(true)
                    };

                    pwm_in.timer.sr.modify(|_, w| {
                        w.uif().clear_bit();
                        w
                    });
                }

                if pwm_in.timer.sr.read().tif().bit_is_set() {
                    pwm_in.timeout = None;

                    pwm_in.timer.sr.modify(|_, w| {
                        w.tif().clear_bit();
                        w
                    });
                }
            }
        }
    });
}

#[entry]
fn main() -> ! {
    let mut peripherals = stm32g07x::Peripherals::take().unwrap();
    let mut core_peripherals = stm32g07x::CorePeripherals::take().unwrap();

    peripherals
        .RCC
        .iopenr
        .write(|w| {
            w.iopben().set_bit();
            w.iopaen().set_bit();
            w
        });

    peripherals.RCC.apbenr1.write(|w| {
        w.tim3en().set_bit();
        w
    });

    core_peripherals.SYST.set_reload(0x00ffffff);
    core_peripherals.SYST.clear_current();
    core_peripherals.SYST.enable_counter();

    let mut rcc = peripherals.RCC.constrain();
    let gpioa = peripherals.GPIOA.split(&mut rcc);
    let gpiob = peripherals.GPIOB.split(&mut rcc);

    //let uart_config = Config::default().baudrate(115200.bps());

    //let mut uart = peripherals
        //.USART1
        //.usart(gpiob.pb6, gpiob.pb7, uart_config, &mut rcc)
        //.unwrap();

    let mut uart = uart::Uart::setup(
        peripherals.USART1,
        gpiob.pb6,
        gpiob.pb7,
    );

    writeln!(uart, "start").ok();

    init_pwm(gpioa.pa6, peripherals.TIM3);

    bldc::init(
        gpioa.pa7.into_push_pull_output(),
        gpioa.pa8,
        gpioa.pa0.into_floating_input(),
        gpiob.pb0.into_push_pull_output(),
        gpioa.pa9,
        gpioa.pa1.into_floating_input(),
        gpiob.pb1.into_push_pull_output(),
        gpioa.pa10,
        gpioa.pa2.into_floating_input(),
        peripherals.TIM1,
        peripherals.EXTI,
    );

    bldc::duty_cycle(00);
    bldc::run();

    loop {

        //if let Ok((line, len)) = uart.read_line() {
            //let line = str::from_utf8(&line[0..len-1]).ok();
            //writeln!(uart, "{}: {:?}", len, line).ok();

            //if let Some(line) = line {
                //let speed: Result<u16, _> = line.parse();
                //writeln!(uart, "{:?}", speed).ok();
                //if let Ok(speed) = speed {
                    //bldc::duty_cycle(speed);
                    //duty_cycle = speed;
                //}
            //}
        //}

        let duty_cycle = match read_pwm() {
            PwmInRead::Low => 0,
            PwmInRead::High => 255,
            PwmInRead::Pwm { period, high_time } => {
                if period > 0 {
                    ((high_time as u64 * 255 * RANGE_DUTY as u64) / (period as u64)).saturating_sub(MIN_DUTY as u64 * RANGE_DUTY as u64).min(255) as u16
                } else {
                    0
                }
            }
        };

        bldc::duty_cycle(duty_cycle);

        //bldc::run();
        if uart.tx_len() == Ok(0) {
            let time = SYST::get_reload() - SYST::get_current();
            core_peripherals.SYST.clear_current();
            let counts = bldc::count();

            if let Some(counts) = counts {
                let speed = counts * 100000 / time;
                writeln!(uart, "{} : {} | {} : {}", count_pwm(), duty_cycle, speed, counts).ok();
            }
        }
    }
}
