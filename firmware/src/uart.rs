use core::fmt;
use core::fmt::Write;

use core::cell::RefCell;

use cortex_m::interrupt::Mutex;
use cortex_m_rt_macros::interrupt as isr;

use stm32g0::stm32g07x;
use stm32g0::stm32g07x::Interrupt as interrupt;
use stm32g0::stm32g07x::NVIC;
use stm32g0::stm32g07x::RCC;

use stm32g0xx_hal::gpio::gpiob::{PB6, PB7};
use stm32g0xx_hal::gpio::DefaultMode;
use stm32g0xx_hal::serial::{TxPin, RxPin};

pub const RX_BUFFER_LEN: usize = 64;
pub const TX_BUFFER_LEN: usize = 2048;

struct Buffer<T> {
    bytes: T,
    len: usize,
}

static UART: Mutex<RefCell<Option<stm32g07x::USART1>>> = Mutex::new(RefCell::new(None));

static RX_BUF: Mutex<RefCell<Buffer<[u8; RX_BUFFER_LEN]>>> =
    Mutex::new(RefCell::new(Buffer {
        bytes: [0; RX_BUFFER_LEN],
        len: 0,
    }));

static TX_BUF: Mutex<RefCell<Buffer<[u8; TX_BUFFER_LEN]>>> =
    Mutex::new(RefCell::new(Buffer {
        bytes: [0; TX_BUFFER_LEN],
        len: 0,
    }));

#[derive(PartialEq)]
pub enum TxError {
    BufferFull,
    Busy,
    NotInitialized,
}

#[derive(PartialEq)]
pub enum RxError {
    BufferEmpty,
    Busy,
    NotInitialized,
}

pub struct Uart {}

impl Uart {
    pub fn setup(
        uart: stm32g07x::USART1,
        tx: PB6<DefaultMode>,
        rx: PB7<DefaultMode>,
    ) -> Uart {
        let rcc = unsafe { &(*RCC::ptr()) };

        // enable clock for usart
        rcc.apbenr2.modify(|_, w| w.usart1en().set_bit());

        TxPin::setup(&tx);
        RxPin::setup(&rx);

        // set buadrate

        // 9600 baud (16MHz APB2)
        //uart.brr.write(|w| unsafe { w.bits(0x683) });

        // 115200 baud (16MHz APB2)
        uart.brr.write(|w| unsafe { w.bits(0x008b) });

        // 230400 baud (16MHz APB2)
        //uart.brr.write(|w| unsafe { w.bits(0x0045) });

        // 2M baud doesn't seem to work (16MHz APB2)
        //uart.cr1.write(|w| w.over8().set_bit());
        //uart.brr.write(|w| unsafe { w.bits(0x0010) });

        // 230400 baud (84MHz APB2)
        //uart.brr.write(|w| unsafe { w.bits(0x016d) });

        // 115200 baud (84MHz APB2)
        //uart.brr.write(|w| unsafe { w.bits(0x02d9) });

        // enable rx and tx
        uart.cr1.write(|w| {
            w.ue()
                .set_bit()
                .re()
                .set_bit()
                .te()
                .set_bit()
                .rxneie()
                .set_bit()
                .tcie()
                .set_bit()
        });

        cortex_m::interrupt::free(|cs| UART.borrow(cs).replace(Some(uart)));

        unsafe {
            NVIC::unmask(interrupt::USART1);
        }

        Uart {}
    }

    fn add_byte(&mut self, c: u8) -> Result<(), TxError> {
        cortex_m::interrupt::free(|cs| {
            if let Ok(mut buf) = TX_BUF.borrow(cs).try_borrow_mut() {
                if buf.len < buf.bytes.len() {
                    if buf.len == 0 {
                        if let Some(uart) = UART.borrow(cs).borrow().as_ref() {
                            uart.tdr.write(|w| unsafe { w.tdr().bits(c as u16) });
                            buf.len = 1;
                            Ok(())
                        } else {
                            Err(TxError::Busy)
                        }
                    } else {
                        for i in (1..buf.len).rev() {
                            buf.bytes[i] = buf.bytes[i - 1];
                        }
                        buf.bytes[0] = c;
                        buf.len += 1;

                        Ok(())
                    }
                } else {
                    Err(TxError::BufferFull)
                }
            } else {
                Err(TxError::Busy)
            }
        })
    }

    pub fn add_bytes(&mut self, b: &[u8]) -> Result<(), TxError> {
        for &c in b {
            self.add_byte(c)?;
        }

        Ok(())
    }

    pub fn add_str(&mut self, s: &str) -> Result<(), TxError> {
        for &c in s.as_bytes() {
            self.add_byte(c)?;
        }

        Ok(())
    }

    pub fn clear_tx(&mut self) -> Result<(), TxError> {
        cortex_m::interrupt::free(|cs| {
            if let Ok(mut buf) = TX_BUF.borrow(cs).try_borrow_mut() {
                buf.len = 0;
                buf.bytes = [0; TX_BUFFER_LEN];
                Ok(())
            } else {
                Err(TxError::Busy)
            }
        })
    }

    pub fn clear_rx(&mut self) -> Result<(), TxError> {
        cortex_m::interrupt::free(|cs| {
            if let Ok(mut buf) = RX_BUF.borrow(cs).try_borrow_mut() {
                buf.len = 0;
                buf.bytes = [0; RX_BUFFER_LEN];
                Ok(())
            } else {
                Err(TxError::Busy)
            }
        })
    }

    pub fn tx_len(&self) -> Result<usize, TxError> {
        cortex_m::interrupt::free(|cs| {
            if let Ok(buf) = TX_BUF.borrow(cs).try_borrow().as_ref() {
                Ok(buf.len)
            } else {
                Err(TxError::Busy)
            }
        })
    }

    pub fn rx_len(&self) -> Result<usize, RxError> {
        cortex_m::interrupt::free(|cs| {
            if let Ok(buf) = RX_BUF.borrow(cs).try_borrow().as_ref() {
                Ok(buf.len)
            } else {
                Err(RxError::Busy)
            }
        })
    }

    pub fn read_byte(&mut self) -> Result<u8, RxError> {
        cortex_m::interrupt::free(|cs| {
            if let Ok(mut buf) = RX_BUF.borrow(cs).try_borrow_mut() {
                if buf.len > 0 {
                    let c = buf.bytes[0];
                    for i in 1..buf.len {
                        buf.bytes[i - 1] = buf.bytes[i];
                    }
                    buf.len -= 1;
                    let len = buf.len;
                    buf.bytes[len] = 0;
                    Ok(c)
                } else {
                    Err(RxError::BufferEmpty)
                }
            } else {
                Err(RxError::Busy)
            }
        })
    }

    pub fn read_line(&mut self) -> Result<([u8; RX_BUFFER_LEN], usize), RxError> {
        cortex_m::interrupt::free(|cs| {
            if let Ok(mut buf) = RX_BUF.borrow(cs).try_borrow_mut() {
                if buf.len > 0 && buf.bytes[buf.len - 1] == 0x0A {
                    let new_buf = buf.bytes.clone();
                    let new_len = buf.len;
                    buf.bytes = [0; RX_BUFFER_LEN];
                    buf.len = 0;
                    Ok((new_buf, new_len))
                } else {
                    Err(RxError::BufferEmpty)
                }
            } else {
                Err(RxError::Busy)
            }
        })
    }

    pub fn read_exact(&mut self, fill_buf: &mut [u8]) -> Result<(), RxError> {
        cortex_m::interrupt::free(|cs| {
            if let Ok(mut buf) = RX_BUF.borrow(cs).try_borrow_mut() {
                if buf.len >= fill_buf.len() {
                    fill_buf.clone_from_slice(&buf.bytes[0..fill_buf.len()]);
                    buf.len -= fill_buf.len();
                    Ok(())
                } else {
                    Err(RxError::BufferEmpty)
                }
            } else {
                Err(RxError::Busy)
            }
        })
    }
}

impl Write for Uart {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.add_str(s).map(|_| ()).map_err(|_| fmt::Error)
    }
}

#[allow(non_snake_case)]
#[isr]
fn USART1() {
    cortex_m::interrupt::free(|cs| {
        if let Some(uart) = UART.borrow(cs).borrow().as_ref() {
            if uart.isr.read().rxne().bit() {
                let rx = uart.rdr.read().rdr().bits() as u8;
                //uart.dr.write(|w| w.dr().bits(rx as u16));
                if let Ok(mut buf) = RX_BUF.borrow(cs).try_borrow_mut() {
                    if buf.len < RX_BUFFER_LEN {
                        let len = buf.len;
                        buf.bytes[len] = rx;
                        buf.len += 1;
                    }
                }
            }

            if uart.isr.read().tc().bit() {
                if let Ok(mut buf) = TX_BUF.borrow(cs).try_borrow_mut() {
                    // decrement the byte we just send
                    if buf.len > 0 {
                        buf.len -= 1;
                    }

                    // send the next byte
                    if buf.len > 0 {
                        let index = buf.len - 1;
                        uart.tdr.write(|w| unsafe { w.tdr().bits(buf.bytes[index] as u16) });
                        buf.bytes[index] = 0;
                    }
                }

                uart.icr.write(|w| w.tccf().set_bit());
                //uart.isr.modify(|_, w| w.tc().clear_bit());
            }
        }
    });
}
